﻿function fetchXml(entityName, fetchXmlText) {
    return new Promise(function (resolve, reject) {

        //Converting fetchXML to URI.
        encodedFetchXml = encodeURI(fetchXmlText);

        //Assigning query to query path. entityName is located in fetchXml function.
        queryPath = "/api/data/v9.0/" + entityName + "?fetchXml=" + encodedFetchXml;

        //Returns the base URL that was used to access the application.
        requestPath = Xrm.Page.context.getClientUrl() + queryPath;

        //Use XMLHttpRequest (XHR) objects to interact with servers. 
        req = new XMLHttpRequest();

        //Initializes a request.
        req.open("GET", requestPath, true);

        //Sets the value of an HTTP request header?
        req.setRequestHeader("Accept", "application/json");

        req.setRequestHeader("Content-Type", "application/json; charset=utf-8");

        req.onload = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    resolve(req.response);

                } else {
                    reject({
                        status: this.status,
                        statusText: req.statusText
                    });
                }
            }
        }
        req.onerror = function () {
            reject({
                status: this.status,
                statusText: req.statusText
            });
        }
        req.send();
    });
}

function notes(executionContext) {
    debugger;
    var formContext = executionContext.getFormContext(),
        inspectionId = formContext.data.entity.getId(),

        //query to find inspection with notes
   fetchXmlText = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>" +
    "  <entity name='gscserv_inspection'>" +
    "    <attribute name='gscserv_inspectionid' />" +
    "    <attribute name='gscserv_name' />" +
    "    <order attribute='gscserv_name' descending='false' />" +
    "    <link-entity name='annotation' from='objectid' to='gscserv_inspectionid' link-type='inner' alias='ai'>" +
    "      <filter type='and'>" +
    "        <condition attribute='subject' operator='like' value='%Retur%' />" +
    "      </filter>" +
    "    </link-entity>" +
    "  </entity>" +
    "</fetch>";

    //return data from query
    fetchXml("gscserv_inspections", fetchXmlText).then(function (data) {
        var returnedData = JSON.parse(data);
        console.log(returnedData);

        //If no Notes are on the page, show error
        if (returnedData.value.length === 0) {
            console.log("Made it");
        } else {
            return;
        }
        

    })
}