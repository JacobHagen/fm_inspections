﻿function hideBpfFields(executionContext) {
    var formContext = executionContext.getFormContext(),
        ownerOfLoan = formContext.getControl("gscserv_leadinvestor").getAttribute().getValue(),
        inspectionType = formContext.getControl("gscserv_internalexternalinspection").getAttribute().getValue(),
        activeProcess = formContext.data.process.getActiveProcess().getName();

    //Need to go back and make these arrays to make it cleaner

    //Causes the form to change if the loan changes in case the Lead Investor changes. 
    setTimeout(function () {
        if (formContext.data.entity.attributes.get("gscserv_loan").getIsDirty(true)) {
            formContext.data.save();
        };
    }, 400);



    //Hides and shows fields for Under Review BPF given the Lead Investor
    if (activeProcess === "Under Review Business Process Flow") {

        formContext.getControl("header_process_gscserv_leadinvestor").setVisible(false);
        formContext.getControl("header_process_gscserv_isthisaseniorsloan").setVisible(false);

        if (ownerOfLoan === "FMA") {
            formContext.getControl("header_process_gscserv_minu").setVisible(false);
            formContext.getControl("header_process_gscserv_minimumofvacantunitstoinspect").setVisible(false);
            formContext.getControl("header_process_gscserv_rentrolldate").setVisible(false);
            formContext.getControl("header_process_gscserv_rentrolloccupancy").setVisible(false);
            formContext.getControl("header_process_gscserv_isthisaseniorsloan_1").setVisible(false);
        }
        else if (ownerOfLoan === "FMC" || ownerOfLoan === "LIF") {
            formContext.getControl("header_process_gscserv_minu").setVisible(true);
            formContext.getControl("header_process_gscserv_minimumofvacantunitstoinspect").setVisible(true);
            formContext.getControl("header_process_gscserv_rentrolldate").setVisible(true);
            formContext.getControl("header_process_gscserv_rentrolloccupancy").setVisible(true);
        }  else {
            formContext.getControl("header_process_gscserv_minu").setVisible(false);
            formContext.getControl("header_process_gscserv_minimumofvacantunitstoinspect").setVisible(false);
            formContext.getControl("header_process_gscserv_rentrolldate").setVisible(false);
            formContext.getControl("header_process_gscserv_rentrolloccupancy").setVisible(false);
        }
    };

    if (activeProcess === "Final Review Business Process Flow") {
        debugger;
        formContext.getControl("header_process_gscserv_leadinvestor").setVisible(false);

        if (ownerOfLoan === "FMA") {
            formContext.getControl("header_process_gscserv_isthisaseniorsloan").setVisible(false);
        } else if (ownerOfLoan === "FMC") {
            formContext.getControl("header_process_gscserv_isthisaseniorsloan_1").setVisible(false);
        } else {
            formContext.getControl("header_process_gscserv_isthisaseniorsloan").setVisible(false);
        }
        if (ownerOfLoan === "FMA" || ownerOfLoan === "PSN") {
            var missingRentRoll = formContext.getControl("header_process_gscserv_missingrentroll").getAttribute().setValue(100000001);
            formContext.data.save();
        }
    };
    
        //Hides and shows fields for Submission BPF given the Lead Investor
        if (activeProcess === "Submission Business Process Flow") {

            if (ownerOfLoan === "FMA") {
                formContext.getControl("header_process_gscserv_missingrentrollemailsenttofreddiemac").setVisible(false);
                formContext.getControl("header_process_gscserv_notenoughunitsinspectedemailsenttofma").setVisible(false);
                formContext.getControl("header_process_gscserv_emailtofanniemaeif45rating").setVisible(true);
            }
            else if (ownerOfLoan === "FMC") {
                formContext.getControl("header_process_gscserv_emailtofanniemaeif45rating").setVisible(false);
                formContext.getControl("header_process_gscserv_missingrentrollemailsenttofreddiemac").setVisible(true);
                formContext.getControl("header_process_gscserv_notenoughunitsinspectedemailsenttofma").setVisible(true);
            } else {
                formContext.getControl("header_process_gscserv_missingrentrollemailsenttofreddiemac").setVisible(false);
                formContext.getControl("header_process_gscserv_notenoughunitsinspectedemailsenttofma").setVisible(false);
                formContext.getControl("header_process_gscserv_emailtofanniemaeif45rating").setVisible(false);
            }
        };

        //Hides and Shows Resumission Tab given status of Resubmission Required
        var resubmissionRequired = formContext.getControl("gscserv_resubmissionrequired").getAttribute().getValue();

        //If resubmission is not required
        if (resubmissionRequired === 100000000) {
            formContext.ui.tabs.get("Submission").sections.get("Resubmission").setVisible(false);
        }
            //Resubmission Required 
        else {
            formContext.ui.tabs.get("Submission").sections.get("Resubmission").setVisible(true);
        }
        //Hides and shows fields related to the BPFs in the Under Review and Final Review tabs on the form.

        if (ownerOfLoan === "FMA") {

            formContext.getControl("gscserv_missingrentrollemailsenttofreddiemac").setVisible(false);
            formContext.getControl("gscserv_notenoughunitsinspectedemailsenttofma").setVisible(false);
            formContext.getControl("gscserv_aretheredmitemsopenfromlastinspection").setVisible(false);
            formContext.getControl("gscserv_emailtofanniemaeif45rating").setVisible(true);
            formContext.getControl("gscserv_minu").setVisible(false);
            formContext.getControl("gscserv_minimumofvacantunitstoinspect").setVisible(false);
            formContext.getControl("gscserv_prsupdatedwithcureddefrepairs").setVisible(false);
            formContext.getControl("gscserv_rentrolldate1").setVisible(false);
            formContext.getControl("gscserv_rentrolloccupancy").setVisible(false);
            formContext.getControl("gscserv_rentrolldate2").setVisible(false);
            formContext.getControl("gscserv_rentrolloccupancy1").setVisible(false);
            formContext.ui.tabs.get("UnderReview").sections.get("UnderReview_FMA").setVisible(true);
            formContext.ui.tabs.get("FinalReview").sections.get("FinalReview_FMA").setVisible(true);

        } else if (ownerOfLoan === "FMC" || ownerOfLoan === "LIF") {
            formContext.getControl("gscserv_missingrentrollemailsenttofreddiemac").setVisible(true);
            formContext.getControl("gscserv_notenoughunitsinspectedemailsenttofma").setVisible(true);
            formContext.getControl("gscserv_aretheredmitemsopenfromlastinspection").setVisible(true);
            formContext.getControl("gscserv_emailtofanniemaeif45rating").setVisible(false);
            formContext.getControl("gscserv_minu").setVisible(true);
            formContext.getControl("gscserv_minimumofvacantunitstoinspect").setVisible(true);
            formContext.getControl("gscserv_prsupdatedwithcureddefrepairs").setVisible(true);
            formContext.getControl("gscserv_rentrolldate1").setVisible(true);
            formContext.getControl("gscserv_rentrolloccupancy").setVisible(true);
            formContext.getControl("gscserv_rentrolldate2").setVisible(true);
            formContext.getControl("gscserv_rentrolloccupancy1").setVisible(true);
            formContext.ui.tabs.get("UnderReview").sections.get("UnderReview_FMA").setVisible(false);
            formContext.ui.tabs.get("FinalReview").sections.get("FinalReview_FMA").setVisible(false);
        } else {
            formContext.getControl("gscserv_missingrentrollemailsenttofreddiemac").setVisible(false);
            formContext.getControl("gscserv_notenoughunitsinspectedemailsenttofma").setVisible(false);
            formContext.getControl("gscserv_aretheredmitemsopenfromlastinspection").setVisible(false);
            formContext.getControl("gscserv_emailtofanniemaeif45rating").setVisible(false);
            formContext.getControl("gscserv_minu").setVisible(false);
            formContext.getControl("gscserv_minimumofvacantunitstoinspect").setVisible(false);
            formContext.getControl("gscserv_prsupdatedwithcureddefrepairs").setVisible(false);
            formContext.getControl("gscserv_rentrolldate1").setVisible(false);
            formContext.getControl("gscserv_rentrolloccupancy").setVisible(false);
            formContext.getControl("gscserv_rentrolldate2").setVisible(false);
            formContext.getControl("gscserv_rentrolloccupancy1").setVisible(false);
            formContext.ui.tabs.get("UnderReview").sections.get("UnderReview_FMA").setVisible(false);
            formContext.ui.tabs.get("FinalReview").sections.get("FinalReview_FMA").setVisible(false);
        }
    }


function onLoad(executionContext) {
    var formContext = executionContext.getFormContext();

    setTimeout(function () { hideBpfFields(executionContext); }, 2000);
}