﻿function setRequired(executionContext) {

    var formContext = executionContext.getFormContext(),
        leadInvestor = formContext.getAttribute("gscserv_leadinvestor").getValue(),
        activeProcess = formContext.data.process.getActiveProcess().getName();

    //Loan owner is Fannie Mae and BPF is in Submission Stage
    if (leadInvestor === "FMA" && activeProcess === "Submission Business Process Flow") {
        formContext.getControl("header_process_gscserv_emailtofanniemaeif45rating").setRequiredLevel("required");
        formContext.getControl("header_process_gscserv_missingrentrollemailsenttofreddiemac").setRequiredLevel("none");
        formContext.getControl("header_process_gscserv_notenoughunitsinspectedemailsenttofma").setRequiredLevel("none");
    } 
        //Loan owner is Fannie Mae and BPF is in Submission Stage
    else if (leadInvestor === "FMC" && activeProcess === "Submission Business Process Flow") {
        formContext.getControl("header_process_gscserv_emailtofanniemaeif45rating").setRequiredLevel("none");
        formContext.getControl("header_process_gscserv_missingrentrollemailsenttofreddiemac").setRequiredLevel("required");
        formContext.getControl("header_process_gscserv_notenoughunitsinspectedemailsenttofma").setRequiredLevel("required");
    }
    else {
        formContext.getControl("header_process_gscserv_emailtofanniemaeif45rating").setRequiredLevel("none");
        formContext.getControl("header_process_gscserv_missingrentrollemailsenttofreddiemac").setRequiredLevel("none");
        formContext.getControl("header_process_gscserv_notenoughunitsinspectedemailsenttofma").setRequiredLevel("none");
    }

}