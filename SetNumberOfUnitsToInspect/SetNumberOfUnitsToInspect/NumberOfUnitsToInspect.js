﻿function setNumberOfUnitsToInspect(executionContext) {
    debugger;
    var formContext = executionContext.getFormContext(),
        fmaUnits = formContext.getControl("gscserv_minimumunitstoinspectfma").getAttribute().getValue(),
        fmcUnits = formContext.getControl("gscserv_minimumunitstoinspectfmc").getAttribute().getValue(),
        leadInvestor = formContext.getControl("gscserv_leadinvestor").getAttribute().getValue(),
        totalUnitsValue = formContext.getControl("gscserv_totalunits").getAttribute().getValue(),
        totalOccupied = formContext.getControl("gscserv_totaloccupiedunits").getAttribute().getValue(),
        totalVacant = formContext.getControl("gscserv_totalvacantunits").getAttribute().getValue(),
        occupied = "gscserv_minu",
        vacant = "gscserv_minimumofvacantunitstoinspect",
        totalUnits = "gscserv_totalofunitstoinspect",
        occupiedValue,
        vacantValue;

    if (leadInvestor === "FMA") {
        if (fmaUnits !== null) {
            formContext.getAttribute(totalUnits).setValue(fmaUnits);
        }

    } else if (leadInvestor === "FMC") {
        occupiedValue = Math.ceil(totalOccupied * 0.03);
        vacantValue = Math.ceil(totalVacant * 0.2);

        formContext.getAttribute(occupied).setValue(occupiedValue);
        formContext.getAttribute(vacant).setValue(vacantValue);

        var minOccupiedUnits = formContext.getControl(occupied).getAttribute().getValue(),
            minVacantUnits = formContext.getControl(vacant).getAttribute().getValue();

         if (minOccupiedUnits === 0) {
            formContext.getAttribute(occupied).setValue(0);
            } else if (minOccupiedUnits !== null && minOccupiedUnits < 3) {
                formContext.getAttribute(occupied).setValue(3);
            } else if (minOccupiedUnits !== null && minOccupiedUnits > 15) {
                formContext.getAttribute(occupied).setValue(15);
            }  else {
                formContext.getAttribute(occupied).setValue(minOccupiedUnits);
            }
        
         if (minVacantUnits === 0) {
             formContext.getAttribute(vacant).setValue(0);
            } else if (minVacantUnits !== null && minVacantUnits < 3) {
                formContext.getAttribute(vacant).setValue(3);
            } else if (minVacantUnits !== null && minVacantUnits > 15) {
                formContext.getAttribute(vacant).setValue(15);
            }  else {
                formContext.getAttribute(vacant).setValue(minVacantUnits);
            }
        }

    if (fmcUnits !== null) {
        formContext.getAttribute(totalUnits).setValue(fmcUnits);
    }
}