﻿function averageRating(executionContext) {
    debugger;
    var formContext = executionContext.getFormContext(),
        activeProcess = formContext.data.process.getActiveProcess().getName(),

        //FMA Ratings for Under Review Business Process Flow
        lifeSafetyRating = formContext.getAttribute("gscserv_lifesafetyrating").getValue(),
        deferredMaintenenceRating = formContext.getAttribute("gscserv_deferredmaintenancerating").getValue(),
        routineMaintenenceRating = formContext.getAttribute("gscserv_routinemaintenancerating").getValue(),
        capitalImprovementRating = formContext.getAttribute("gscserv_capitalimprovementrating").getValue(),
        levelVolumeRating = formContext.getAttribute("gscserv_levelvolumeofissuesrating").getValue(),

        ratings = [lifeSafetyRating, deferredMaintenenceRating, routineMaintenenceRating, capitalImprovementRating, levelVolumeRating],

        //FMA Ratings for Final Review Business Process Flow
        lifeSafetyRating2 = formContext.getAttribute("gscserv_lifesafetyrating2").getValue(),
        deferredMaintenenceRating2 = formContext.getAttribute("gscserv_deferredmaintanencerating2").getValue(),
        routineMaintenenceRating2 = formContext.getAttribute("gscserv_routinemaintenancerating2").getValue(),
        capitalImprovementRating2 = formContext.getAttribute("gscserv_capitalimprovementrating2").getValue(),
        levelVolumeRating2 = formContext.getAttribute("gscserv_levelvolumeofissuesrating2").getValue(),

        ratings2 = [lifeSafetyRating2, deferredMaintenenceRating2, routineMaintenenceRating2, capitalImprovementRating2, levelVolumeRating2];

    //for loop to look for data in all FMA Ratings in the Under Review Business Process Flow
        if (activeProcess === "Under Review Business Process Flow") {
            var total = 0;

            for (var i = 0; i < ratings.length; i++) {
                if (ratings[i] === null) {
                    return;
                } else if (ratings[i] !== null) {
                    total += ratings[i];

                }
            }
            var averageRating = total / ratings.length;

            formContext.getAttribute("gscserv_averageoverallrating").setValue(averageRating);
        }

    //for loop to look for data in all FMA Ratings in the Under Review Business Process Flow
        if (activeProcess === "Final Review Business Process Flow") {
            var total2 = 0;

            for (var i = 0; i < ratings2.length; i++) {
                if (ratings2[i] === null) {
                    return;
                } else if (ratings2[i] !== null) {
                    total2 += ratings2[i];

                }
            }
            var averageRating2 = total2 / ratings2.length;

            formContext.getAttribute("gscserv_averageoverallrating2").setValue(averageRating2);
        }
}