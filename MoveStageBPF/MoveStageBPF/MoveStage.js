﻿function moveNextStage(executionContext) {
    debugger;
    var formContext = executionContext.getFormContext(), //Get formContext
        activeProcess = formContext.data.process.getActiveProcess(); //Get Active Process

    // Get Process Name
    if (activeProcess !== null) {

        // Get Active Stage
            var moveToNext = true,
                stage = formContext.data.process.getActiveStage();

                var stepsCollection = stage.getSteps();
                console.log(stepsCollection);

                //loop on the stepsColletion
                for (var i = 0; i < stepsCollection.getLength() ; ++i) {
                    // Returns the logical name of the attribute associated to the step.
                    var stepAttributeName = stepsCollection.get(i).getAttribute();

                    // Retrieves the data value for an attribute.
                    var attributeValue = Xrm.Page.getAttribute(stepAttributeName).getValue();
                    console.log(attributeValue);

                    if ((attributeValue == null) || (attributeValue == false)) {
                        moveToNext = false;
                    }
                }
                if (moveToNext) {
                    formContext.data.process.moveNext();
                }
    }
};
