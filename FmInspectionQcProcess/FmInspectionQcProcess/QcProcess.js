﻿/*============================================================================================================================
 * File Name: FmInspectionsQcProcess.js
 * Company: Greystone IT
 *
 *
 * Handles when a user clicks the Ready for QC button in the FM Inspection Application within Microsoft Dynamics. 
 * Triggers the Ready for QC workflow.
 *
 *
 * Authors: Jacob Hagen      (jacob.hagen@greyco.com)
 * 
 * Version: 1.0
 * Dependencies: N/A
 *
 *
============================================================================================================================*/


/*
 * Purpose: Handles calculating the Days with AM after Ready for QC has been clicked.
 * Inputs: beginDate, endDate
	param: beginDate [string] - Represents the first date as a string
	param: endDate [string] - Represents the second date as a string
*/
function dateDiffDays(beginDate, endDate) {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

    // We are setting hours here to make sure we are always comparing at midnight.
    beginDate = new Date(beginDate.setHours(0, 0, 0)),
	endDate = new Date(endDate.setHours(0, 0, 0));

    return Math.round(Math.abs((beginDate.getTime() - endDate.getTime()) / (oneDay)));
}


/*
 * Purpose:  Checks to see if the user has the proper credentials to complete the QC process.
 * Returns:  {BOOLEAN} A boolean value representing the result of the permissions check.
*/
function checkPermissions(role) {
    var permissionsResult = false,
        currentUser = Xrm.Page.context.getUserName(),
        user = null;
    //analyst = Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue() !== null ? Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()[0].name : null;
    //assignedAM = Xrm.Page.getAttribute("gscserv_assignedam").getValue() !== null ? Xrm.Page.getAttribute("gscserv_assignedam").getValue()[0].name : null;
    //seniorAM = Xrm.Page.getAttribute("gscserv_senioram").getValue() !== null ? Xrm.Page.getAttribute("gscserv_senioram").getValue()[0].name : null;

    switch (role) {
        case "Analyst":
            user = Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue() !== null ? Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()[0].name : null;
            break;
        case "Assigned AM":
            user = Xrm.Page.getAttribute("gscserv_assignedam").getValue() !== null ? Xrm.Page.getAttribute("gscserv_assignedam").getValue()[0].name : null;
            break;
        case "Senior AM":
            user = Xrm.Page.getAttribute("gscserv_senioram").getValue() !== null ? Xrm.Page.getAttribute("gscserv_senioram").getValue()[0].name : null;
            break;
        case "Senior Reviewer":
            user = Xrm.Page.getAttribute("gscserv_seniorreviewer").getValue() !== null ? Xrm.Page.getAttribute("gscserv_seniorreviewer").getValue()[0].name : null;
            break;
        default:
            user = null;
            break;
    }
    // Check to make sure we have an Assigned Reviewer. Then check to see that they are the logged in user.
    if (role !== null) {
        if (user === currentUser) {
            permissionsResult = true;
        }
    }

    return permissionsResult;
}


/*
 * Purpose: To display an error if the runWorkflow
*/
function displayError() {
    alert(this.status);
}


/*
 * Purpose:  Calls a workflow from JavaScript and executes it in Dynamics.
 * Inputs:   req, successCallback, errorCallback
    @param {object} [req] An object holding the request response.
    @param {function} [successCallback] The function that will run on successful workflow execution.
    @param {function} [errorCallback] The function that will run on failure of the workflow execution.
 */
function assignResponse(req, successCallback, errorCallback) {
    if (Number(req.readyState) === 4) {
        if (Number(req.status) === 200) {
            if (successCallback) {
                successCallback();
            }
        } else {
            if (errorCallback) {
                errorCallback();
            }
        }
    }
}


/*
 * Function: setUrl
 * Purpose:  Sets the URL and OrgServicePath based on the current window href.
 * Returns:  {string} The Url that should be used for running Workflow requests.
*/
function setUrl() {
    var orgServicePath = "/XRMServices/2011/Organization.svc/web",
        url = "";

    // Check the URL for the tier name.
    if (window.location.href.indexOf("sam-int.crm") > -1) {
        url = "https://sam-int.crm.dynamics.com";
    }
    else if (window.location.href.indexOf("sam-qa.crm") > -1) {
        url = "https://sam-qa.crm.dynamics.com";
    }
    else if (window.location.href.indexOf("sam-uat.crm") > -1) {
        url = "https://sam-uat.crm.dynamics.com";
    }
    else if (window.location.href.indexOf("sam.crm") > -1) {
        url = "https://sam.crm.dynamics.com";
    }

    return url + orgServicePath;

}


/*
 * Purpose:  Calls a workflow from JavaScript and executes it in Dynamics.
 * Inputs:   workflowId, recordId, successCallback, errorCallback, confirmationPrompt, url
    @param {sting} [workflowId] The ID of the Workflow
    @param {string} [recordId] The ID of the record that the workflow will run against.
    @param {function} [successCallback] The function that will run on successful workflow execution.
    @param {function} [errorCallback] The function that will run on failure of the workflow execution.
 */
function runWorkflow(workflowId, recordId, successCallback, errorCallback, type) {
    var url = setUrl(),
		confirmTitle,
		confirmSubtitle;

    switch (type) {
        case "submit for review":
            confirmTitle = "Submit for Review";
            confirmSubtitle = "Are you sure you want to submit this Inspection for review?";
            break;
        case "return to inspector":
            confirmTitle = "Return to Inspector";
            confirmSubtitle = "Are you sure you want to return this Inspection to the Inspector?";
            break;
        case "return inspection":
            confirmTitle = "Return Inspection";
            confirmSubtitle = "Are you sure you want to return this Inspection to Under Review?";
            break;
        case "return inspection after qc":
            confirmTitle = "Return Inspection";
            confirmSubtitle = "Are you sure you want to return this Inspection to Under Review?";
            break;
        case "approve inspection for final qc":
            confirmTitle = "Approve Inspection for QC";
            confirmSubtitle = "Are you sure you want to approve this Inspection?";
            break;
        case "approve inspection after qc":
            confirmTitle = "Approve Inspection";
            confirmSubtitle = "Are you sure you want to approve this Inspection?";
            break;
        case "approve inspection after review":
            confirmTitle = "Approve Inspection";
            confirmSubtitle = "Are you sure you want to approve this Inspection?";
            break;
        case "approve returned inspection":
            confirmTitle = "Approve Returned Inspection";
            confirmSubtitle = "Are you sure you want to approve this Inspection?";
            break;
        case "submit to manager":
            confirmTitle = "Submit to manager";
            confirmSubtitle = "Are you sure you want to submit this Inspection for Senior Review?";
            break;
        case "return to am":
            confirmTitle = "Return to AM";
            confirmSubtitle = "Are you sure you want to return this Inspection to the AM?";
            break;
        case "approve qc":
            confirmTitle = "Approve QC";
            confirmSubtitle = "Are you sure you want to approve the QC for this Inspection?";
            break;
        case "return to analyst":
            confirmTitle = "Return to Analyst";
            confirmSubtitle = "Are you sure you want to return this Inspection to the Analyst?";
            break;
        case "ready for final review":
            confirmTitle = "Ready for Final Review";
            confirmSubtitle = "Are you sure you want to approve the inspection for final review?";
            break;
        default:
            confirmTitle = "Ooops!";
            confirmSubtitle = "Something went wrong. Would you like to try to continue anyway?";
    }
    var confirmOptions = {
        cancelButtonLabel: "CANCEL",
        confirmButtonLabel: "YES",
        subtitle: confirmSubtitle,
        title: confirmTitle
    }


    // Let's check to make sure they are ready to submit.
    Xrm.Navigation.openConfirmDialog(confirmOptions).then(
		function (success) {
		    if (success.confirmed) {
		        // Launch the processing modal
		        Xrm.Utility.showProgressIndicator("Processing...");

		        // Build the request XML.
		        var request = new XMLHttpRequest(),
                    requestBody = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                            "<s:Body>" +
                                "<Execute xmlns=\"http://schemas.microsoft.com/xrm/2011/Contracts/Services\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                                "<request i:type=\"b:ExecuteWorkflowRequest\" xmlns:a=\"http://schemas.microsoft.com/xrm/2011/Contracts\" xmlns:b=\"http://schemas.microsoft.com/crm/2011/Contracts\">" +
                                    "<a:Parameters xmlns:c=\"http://schemas.datacontract.org/2004/07/System.Collections.Generic\">" +
                                    "<a:KeyValuePairOfstringanyType>" +
                                        "<c:key>EntityId</c:key>" +
                                        "<c:value i:type=\"d:guid\" xmlns:d=\"http://schemas.microsoft.com/2003/10/Serialization/\">" + recordId + "</c:value>" +
                                    "</a:KeyValuePairOfstringanyType>" +
                                    "<a:KeyValuePairOfstringanyType>" +
                                        "<c:key>WorkflowId</c:key>" +
                                        "<c:value i:type=\"d:guid\" xmlns:d=\"http://schemas.microsoft.com/2003/10/Serialization/\">" + workflowId + "</c:value>" +
                                    "</a:KeyValuePairOfstringanyType>" +
                                    "</a:Parameters>" +
                                    "<a:RequestId i:nil=\"true\" />" +
                                    "<a:RequestName>ExecuteWorkflow</a:RequestName>" +
                                "</request>" +
                                "</Execute>" +
                            "</s:Body>" +
                            "</s:Envelope>";


		        // Setup the request properties.
		        request.open("POST", url, true);
		        // Responses will return XML. It isn't possible to return JSON.
		        request.setRequestHeader("Accept", "application/xml, text/xml, */*");
		        request.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
		        request.setRequestHeader("SOAPAction", "http://schemas.microsoft.com/xrm/2011/Contracts/Services/IOrganizationService/Execute");
		        request.onerror = displayError;
		        request.onreadystatechange = function () { assignResponse(request, successCallback, errorCallback); };
		        request.send(requestBody);
		    } else {
		        console.log("Request cancelled by user.");
		    }
		},
		function (result) {
		    console.log(result);
		    alert("Something went wrong. Check the console.");
		})
}

/*
 * Purpose:  Handles when a user clicks the Approve Inspection button during the QC process of an FM Inspection.
*/ //Added correct one
function approveInspectionForQc() {
    var workflowId = "D7B9453E-3B79-401F-AC8D-66A3E241706A", // ID of the ReadyForQC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_senioram").getValue()],
        fields = ["gscserv_overallrating", "gscserv_senioram"],
        fieldNames = ["Overall Property Rating", "AM's Manager"],

        //For when Overall Property Rating is less than 3
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        twoOrLowerFields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        twoOrLowerFieldNames = ["Overall Property Rating", "PIPF Analyst"],
        results = [],

    successCallback =
        function () {
            Xrm.Utility.closeProgressIndicator();

            // Refresh the form data.
            Xrm.Page.data.refresh();
        },
    errorCallback =
        function () {
            Xrm.Utility.closeProgressIndicator();

            Xrm.Page.data.entity.save();
            //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 1st one is for when the Overall Property Rating is < 3
            if (requiredFields[0] >= 3) {
                for (var i = 0; i < requiredFields.length; i++) {
                    if (requiredFields[i] === null) {
                        console.log(fieldNames[i]);
                        //Xrm.Page.getControl(fields[i]).setFocus();
                        results.push(fieldNames[i]);
                    }
                }
            } else {
                for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                    if (twoOrLowerRequiredFields[i] === null) {
                        console.log(twoOrLowerFieldNames[i]);
                        //Xrm.Page.getControl(fields[i]).setFocus();
                        results.push(twoOrLowerFieldNames[i]);
                    }
                }
            }


            var resultsString = results.join(", "), //adding comma to array results
                indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
            resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

            if (results.length < 2) {
                Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to approve the inspection for final QC." });
            } else {
                Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to approve the inspection for final QC." });
            }
        }
    type = "approve inspection for final qc";
    // Make a call to run the Ready for QC workflow.

    if (checkPermissions("Assigned AM")) {
        // Make a call to run the Ready for QC workflow.
        var isDirty = Xrm.Page.data.entity.getIsDirty();

        if (isDirty === true) {
            Xrm.Page.data.entity.save();
        }
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    } else {
        Xrm.Navigation.openAlertDialog({ text: "Only the Asset Manager assigned to the Loan can perform this action." });
    }
}



/*
 * Purpose:  Handles when a user clicks the Approve Inspection button during the QC process of aan FM Inspection.
*/ //Added Correct One
function approveInspectionAfterQc() {
    var workflowId = "69253CB5-2001-42A9-803B-67729E204EF3", // ID of the ReadyForQC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue(), Xrm.Page.getAttribute("gscserv_seniorreviewer").getValue()],
        fields = ["gscserv_overallrating", "gscserv_assignedreviewer", "gscserv_seniorreviewer"],
        fieldNames = ["Overall Property Rating", "PIPF Analyst", "Senior Reviewer"],
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        twoOrLowerFields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        twoOrLowerFieldNames = ["Overall Property Rating", "PIPF Analyst"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
                        function () {
                            Xrm.Utility.closeProgressIndicator();
                            //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

                            //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 1st one is for when the Overall Property Rating is < 3
                            if (requiredFields[0] >= 3) {
                                for (var i = 0; i < requiredFields.length; i++) {
                                    if (requiredFields[i] === null) {
                                        console.log(fieldNames[i]);
                                        //Xrm.Page.getControl(fields[i]).setFocus();
                                        results.push(fieldNames[i]);
                                    }
                                }
                            } else {
                                for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                                    if (twoOrLowerRequiredFields[i] === null) {
                                        console.log(twoOrLowerFieldNames[i]);
                                        //Xrm.Page.getControl(fields[i]).setFocus();
                                        results.push(twoOrLowerFieldNames[i]);
                                    }
                                }
                            }

                            var resultsString = results.join(", "), //adding comma to array results
                                indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
                            resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

                            if (results.length < 2) {
                                Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to approve the inspection after QC." });
                            } else {
                                Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to approve the inspection after QC." });
                            }
                        }
    type = "approve inspection after qc";
    // Make a call to run the Ready for QC workflow.

    if (checkPermissions("Analyst")) {
        // Make a call to run the Ready for QC workflow.
        var isDirty = Xrm.Page.data.entity.getIsDirty();

        if (isDirty === true) {
            Xrm.Page.data.entity.save();
        }
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    } else {
        Xrm.Navigation.openAlertDialog({ text: "Only the PIPF Analyst assigned to the Inspection can perform this action." });
    }
}

/*
 * Purpose:  Handles when a user clicks the Approve Inspection button during the QC process of aan FM Inspection.
*/ //Added Correct One
function approveInspectionAfterReview() {
    var workflowId = "B9B9AFA6-B1AF-4FFE-A3CE-35371BB79E99", // ID of the ReadyForQC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue(), Xrm.Page.getAttribute("gscserv_seniorreviewer").getValue()],
        fields = ["gscserv_overallrating", "gscserv_assignedreviewer", "gscserv_seniorreviewer"],
        fieldNames = ["Overall Property Rating", "PIPF Analyst", "Senior Reviewer"],
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        twoOrLowerFields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        twoOrLowerFieldNames = ["Overall Property Rating", "PIPF Analyst"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
          function () {
              Xrm.Utility.closeProgressIndicator();
              //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

              //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 1st one is for when the Overall Property Rating is < 3
              if (requiredFields[0] >= 3) {
                  for (var i = 0; i < requiredFields.length; i++) {
                      if (requiredFields[i] === null) {
                          console.log(fieldNames[i]);
                          //Xrm.Page.getControl(fields[i]).setFocus();
                          results.push(fieldNames[i]);
                      }
                  }
              } else {
                  for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                      if (twoOrLowerRequiredFields[i] === null) {
                          console.log(twoOrLowerFieldNames[i]);
                          //Xrm.Page.getControl(fields[i]).setFocus();
                          results.push(twoOrLowerFieldNames[i]);
                      }
                  }
              }

              var resultsString = results.join(", "), //adding comma to array results
                  indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
              resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

              if (results.length < 2) {
                  Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to approve the inspection after review." });
              } else {
                  Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to approve the inspection after review." });
              }
          }
        type = "approve inspection after review";
    // Make a call to run the Ready for QC workflow.
        if (checkPermissions("Senior Reviewer")) {
            var isDirty = Xrm.Page.data.entity.getIsDirty();

            if (isDirty === true) {
                Xrm.Page.data.entity.save();
            }
        // Make a call to run the Ready for QC workflow.
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    } else {
        Xrm.Navigation.openAlertDialog({ text: "Only the Senior Reviewer assigned to the Inspection can perform this action." });
    }
}

/*
 * Purpose: Handles when a user clicks the aprove qc button during the QC process of an FM Inspection.
*/ //Added Correct One

function approveQc() {
    var workflowId = "2A92127C-719B-4859-8557-B6BD17178A5F", // ID of the Approve QC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        fields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        fieldNames = ["Overall Property Rating", "PIPF Analyst"],
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        twoOrLowerFields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        twoOrLowerFieldNames = ["Overall Property Rating", "PIPF Analyst"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();
                //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

                //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 
                if (requiredFields[0] >= 3) {
                    for (var i = 0; i < requiredFields.length; i++) {
                        if (requiredFields[i] === null) {
                            console.log(fieldNames[i]);
                            //Xrm.Page.getControl(fields[i]).setFocus();
                            results.push(fieldNames[i]);
                        }
                    }
                } else {
                    for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                        if (twoOrLowerRequiredFields[i] === null) {
                            console.log(twoOrLowerFieldNames[i]);
                            //Xrm.Page.getControl(fields[i]).setFocus();
                            results.push(twoOrLowerFieldNames[i]);
                        }
                    }
                }

                var resultsString = results.join(", "), //adding comma to array results
                    indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
                resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

                if (results.length < 2) {
                    Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to approve QC." });
                } else {
                    Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to approve QC." });
                }
            }
        type = "approve qc";

    if (checkPermissions("Senior AM")) {
        // Make a call to run the Ready for QC workflow.
        var isDirty = Xrm.Page.data.entity.getIsDirty();

        if (isDirty === true) {
            Xrm.Page.data.entity.save();
        }
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    }
    else {
        Xrm.Navigation.openAlertDialog({ text: "Only the AM's manager assigned to the Inspection can perform this action." });
    }
}

//Button that Submits for Final QC after Senior AM approves inspection
function readyForFinalReview() {
    var workflowId = "ABC660E3-0B78-44F9-9286-5ECD3BF015B9", // ID of the Approve QC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        fields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        fieldNames = ["Overall Property Rating", "PIPF Analyst"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();
                //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

                //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 

                    for (var i = 0; i < requiredFields.length; i++) {
                        if (requiredFields[i] === null) {
                            console.log(fieldNames[i]);
                            //Xrm.Page.getControl(fields[i]).setFocus();
                            results.push(fieldNames[i]);
                        }
                    }


                var resultsString = results.join(", "), //adding comma to array results
                    indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
                resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

                if (results.length < 2) {
                    Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to approve QC." });
                } else {
                    Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to approve QC." });
                }
            }
    type = "approve qc";

    if (checkPermissions("Analyst")) {
        // Make a call to run the Ready for QC workflow.
        var isDirty = Xrm.Page.data.entity.getIsDirty();

        if (isDirty === true) {
            Xrm.Page.data.entity.save();
        }
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    }
    else {
        Xrm.Navigation.openAlertDialog({ text: "Only PIPF Analaysts can perform this action." });
    }
}

/*
 * Purpose: Handles when a user clicks the Approve Returned Inspection button during the QC process of an FM Inspection.
*/ //Added correct one
function receivedUpdatedInspection() {
    debugger;
    var workflowId = "760A33CA-50FE-4B0E-8BE0-43E0CF155EB8", // ID of the Submit for AM Review Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();
                Xrm.Navigation.openAlertDialog({ text: "Something went wrong. Please try again. If you continue to see this error, please contact support." });
            },
        type = "approve returned inspection";

    var reasonApproved = Xrm.Page.getControl("gscserv_approvereturnedinspectioncomments").getAttribute().getValue();

    // Make a call to run the Ready for QC workflow.
    if (reasonApproved === null) {
        Xrm.Navigation.openAlertDialog({ text: "Approve Returned Inspection Comments field does not contain data." });
    } else {
        // Make a call to run the Ready for QC workflow.
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    }
}

/*
 * Purpose:  Handles when a user clicks the Return Inspection button during the QC process of aan FM Inspection.
*/ //Added Correct One
function returnInspection() {
    var workflowId = "A77F1753-D40E-4A4D-86F7-5109A9AFC2BD", // ID of the Return to AM from Senior Manager Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();
                Xrm.Navigation.openAlertDialog({ text: "Something went wrong. Please try again. If you continue to see this error, please contact support." });
            },
        type = "return inspection";
    // Make a call to run the Ready for QC workflow.
    var reasonReturned = Xrm.Page.getControl("gscserv_reasonreturnedtoexternalinspector").getAttribute().getValue();

    if (reasonReturned === null) {
        Xrm.Navigation.openAlertDialog({ text: "Reason Returned to Inspector does not contain information" });
    } else {
        // Make a call to run the Ready for QC workflow.
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    }
}

/*
 * Purpose:  Handles when a user clicks the Return Inspection button during the QC process of aan FM Inspection.
*/ //Added Correct One
function returnInspectionAfterQC() {
    var workflowId = "F14F12AB-EA0C-4DA4-969B-629678E0CF87", // ID of the ReadyForQC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue(), Xrm.Page.getAttribute("gscserv_senioram").getValue()],
        fields = ["gscserv_overallrating", "gscserv_assignedreviewer", "gscserv_senioram"],
        fieldNames = ["Overall Property Rating", "PIPF Analyst", "AM's Manager"],
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        twoOrLowerFields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        twoOrLowerFieldNames = ["Overall Property Rating", "PIPF Analyst"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
           function () {
               Xrm.Utility.closeProgressIndicator();
               //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

               //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 1st one is for when the Overall Property Rating is < 3
               if (requiredFields[0] >= 3) {
                   for (var i = 0; i < requiredFields.length; i++) {
                       if (requiredFields[i] === null) {
                           console.log(fieldNames[i]);
                           //Xrm.Page.getControl(fields[i]).setFocus();
                           results.push(fieldNames[i]);
                       }
                   }
               } else {
                   for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                       if (twoOrLowerRequiredFields[i] === null) {
                           console.log(twoOrLowerFieldNames[i]);
                           //Xrm.Page.getControl(fields[i]).setFocus();
                           results.push(twoOrLowerFieldNames[i]);
                       }
                   }
               }

               var resultsString = results.join(", "), //adding comma to array results
                   indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
               resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

               if (results.length < 2) {
                   Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to return the inspection after QC." });
               } else {
                   Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to return the inspection after QC." });
               }
           }
        type = "return inspection after qc";
    // Make a call to run the Ready for QC workflow.
    if (checkPermissions("Analyst")) {
        // Make a call to run the Ready for QC workflow.
        var isDirty = Xrm.Page.data.entity.getIsDirty();

        if (isDirty === true) {
            Xrm.Page.data.entity.save();
        }
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    } else {
        Xrm.Navigation.openAlertDialog({ text: "Only the PIPF Analyst assigned to the Inspection can perform this action." });
    }
}

/*
 * Purpose:  Handles when a user clicks the Return to AM button during the QC process of aan FM Inspection.
*/ //Added Correct One
function returnToAm() {
    var workflowId = "A6D26DC5-6C33-4244-A07F-36E80E0AF045", // ID of the ReadyForQC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_senioram").getValue()],
        fields = ["gscserv_overallrating", "gscserv_senioram"],
        fieldNames = ["Overall Property Rating", "AM's Manager"],
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_senioram").getValue()],
        twoOrLowerFields = ["gscserv_overallrating", "gscserv_senioram"],
        twoOrLowerFieldNames = ["Overall Property Rating", "AM's Manager"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
           function () {
               Xrm.Utility.closeProgressIndicator();
               //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

               //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 1st one is for when the Overall Property Rating is < 3
               if (requiredFields[0] >= 3) {
                   for (var i = 0; i < requiredFields.length; i++) {
                       if (requiredFields[i] === null) {
                           console.log(fieldNames[i]);
                           //Xrm.Page.getControl(fields[i]).setFocus();
                           results.push(fieldNames[i]);
                       }
                   }
               } else {
                   for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                       if (twoOrLowerRequiredFields[i] === null) {
                           console.log(twoOrLowerFieldNames[i]);
                           //Xrm.Page.getControl(fields[i]).setFocus();
                           results.push(twoOrLowerFieldNames[i]);
                       }
                   }
               }

               var resultsString = results.join(", "), //adding comma to array results
                   indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
               resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

               if (results.length < 2) {
                   Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to return the inspection to the AM." });
               } else {
                   Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to return the inspection to the AM." });
               }
           }
        type = "return to am";
    // Make a call to run the Ready for QC workflow.

    if (checkPermissions("Senior AM")) {
        // Make a call to run the Ready for QC workflow.
        var isDirty = Xrm.Page.data.entity.getIsDirty();

        if (isDirty === true) {
            Xrm.Page.data.entity.save();
        }
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    } else {
        Xrm.Navigation.openAlertDialog({ text: "Only the AM's manager assigned to the Inspection can perform this action." });
    }
}

/*
 * Purpose:  Handles when a user clicks the Return to Analyst button during the QC process of aan FM Inspection.
*/  //Added Correct One
function returnToAnalyst() {
    var workflowId = "3E025570-7D85-4AAA-8EB8-5F0C7E300B89", // ID of the ApproveRiskMemo Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue(), Xrm.Page.getAttribute("gscserv_seniorreviewer").getValue()],
        fields = ["gscserv_overallrating", "gscserv_assignedreviewer", "gscserv_seniorreviewer"],
        fieldNames = ["Overall Property Rating", "PIPF Analyst", "Senior Reviewer"],
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_assignedreviewer").getValue()],
        twoOrLowerFields = ["gscserv_overallrating", "gscserv_assignedreviewer"],
        twoOrLowerFieldNames = ["Overall Property Rating", "PIPF Analyst"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();

                // Let's save the record then advance the process.
                setTimeout(function () { Xrm.Page.data.save().then(function () { Xrm.Page.data.process.moveNext(); }); }, 750);
            },
        errorCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();
                //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

                //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 1st one is for when the Overall Property Rating is < 3
                if (requiredFields[0] >= 3) {
                    for (var i = 0; i < requiredFields.length; i++) {
                        if (requiredFields[i] === null) {
                            console.log(fieldNames[i]);
                            //Xrm.Page.getControl(fields[i]).setFocus();
                            results.push(fieldNames[i]);
                        }
                    }
                } else {
                    for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                        if (twoOrLowerRequiredFields[i] === null) {
                            console.log(twoOrLowerFieldNames[i]);
                            //Xrm.Page.getControl(fields[i]).setFocus();
                            results.push(twoOrLowerFieldNames[i]);
                        }
                    }
                }
                var resultsString = results.join(", "), //adding comma to array results
                    indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
                resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

                if (results.length < 2) {
                    Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to return the inspection to the Analyst." });
                } else {
                    Xrm.Navigation.openAlertDialog({ text: "The " + resultsString + " fields do not contain data, and are required in order to return the inspection to the Analyst." });
                }
            }
        type = "return to analyst";

    if (checkPermissions("Senior Reviewer")) {
        // Make a call to run the Ready for QC workflow.
        var isDirty = Xrm.Page.data.entity.getIsDirty();

        if (isDirty === true) {
            Xrm.Page.data.entity.save();
        }
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    } else {
        Xrm.Navigation.openAlertDialog({ text: "Only the Senior Reviewer assigned to the Inspection can perform this action." });
    }
}

/*
 * Purpose:  Handles when a user clicks the Return to Inspector button during the QC process of aan FM Inspection.
*/ //Added Correct One

function returnToInspector() {
    var workflowId = "8AA645B4-8C49-41C6-A491-FCD488C3F5E9", // ID of the ReadyForQC Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();
                Xrm.Navigation.openAlertDialog({ text: "Something went wrong. Please try again. If you continue to see this error, please contact support." });
            },
        type = "return to inspector";
    // Make a call to run the Ready for QC workflow.
    var reasonReturned = Xrm.Page.getControl("gscserv_reasonreturnedtoexternalinspector").getAttribute().getValue();

    if (reasonReturned === null) {
        Xrm.Navigation.openAlertDialog({ text: "Reason Returned to Inspector does not contain information" });
    } else {
        // Make a call to run the Ready for QC workflow.
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    }
}

/*
 * Purpose:  Handles when a user clicks the Submit to Manager button during the QC process of aan FM Inspection.
*/ //Added Correct One
function submitToManager() {
    debugger;
    var workflowId = "8B2397F2-A71C-4A1C-B9D7-EA0245606B0A", // ID of the Submit to AM's Manager Workflow in Dynamics. Retrieved from the URL of the Process.		
        recordId = Xrm.Page.data.entity.getId(),
        requiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue(), Xrm.Page.getAttribute("gscserv_senioram").getValue()],
        fields = ["gscserv_overallrating", "gscserv_senioram"],
        fieldNames = ["Overall Property Rating", "AM's Manager"],
        twoOrLowerRequiredFields = [Xrm.Page.getAttribute("gscserv_overallrating").getValue()],
        twoOrLowerFields = ["gscserv_overallrating"],
        twoOrLowerFieldNames = ["Overall Property Rating"],
        results = [],
        successCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();

                // Refresh the form data.
                Xrm.Page.data.refresh();
            },
        errorCallback =
            function () {
                Xrm.Utility.closeProgressIndicator();
                //Xrm.Navigation.openAlertDialog(message, alertOptions).then(function () {});

                //for loop that searches for the required fields. Any fields that are null are returned and pushed to the results array. 1st one is for when the Overall Property Rating is < 3
                if (requiredFields[0] >= 3) {
                    for (var i = 0; i < requiredFields.length; i++) {
                        if (requiredFields[i] === null) {
                            console.log(fieldNames[i]);
                            //Xrm.Page.getControl(fields[i]).setFocus();
                            results.push(fieldNames[i]);
                        }
                    }
                } else {
                    for (var i = 0; i < twoOrLowerRequiredFields.length; i++) {
                        if (twoOrLowerRequiredFields[i] === null) {
                            console.log(twoOrLowerFieldNames[i]);
                            //Xrm.Page.getControl(fields[i]).setFocus();
                            results.push(twoOrLowerFieldNames[i]);
                        }
                    }
                }
                var resultsString = results.join(", "), //adding comma to array results
                    indexedResultsString = resultsString.lastIndexOf(", ");//getting last index of array
                resultsString = resultsString.substring(0, indexedResultsString) + " and " + resultsString.substring(indexedResultsString + 1);//replacing last comma with and

                Xrm.Navigation.openAlertDialog({ text: "The " + results + " field does not contain data, and is required in order to submit the inspection to the AM's Manager." });

                type = "submit to manager";
                // Make a call to run the Ready for QC workflow.

                if (checkPermissions("Assigned AM")) {
                    // Make a call to run the Ready for QC workflow.
                    var isDirty = Xrm.Page.data.entity.getIsDirty();

                    if (isDirty === true) {
                        Xrm.Page.data.entity.save();
                    }
                    runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
                } else {
                    Xrm.Navigation.openAlertDialog({ text: "Only the Asset Manager assigned to the Loan can perform this action." });
                }
            }
}

    /*
     * Purpose: Handles when a user clicks the Submit for Review button during the QC process of an FM Inspection.
    */ //Added correct one
    function submitForReview() {
        var workflowId = "359EE391-163D-4E63-9592-5A8F636ABB5A", // ID of the Submit for AM Review Workflow in Dynamics. Retrieved from the URL of the Process.		
            recordId = Xrm.Page.data.entity.getId(),
            successCallback =
                function () {
                    Xrm.Utility.closeProgressIndicator();

                    // Refresh the form data.
                    Xrm.Page.data.refresh();
                },
            errorCallback =
                function () {
                    Xrm.Utility.closeProgressIndicator();
                    Xrm.Navigation.openAlertDialog({ text: "Something went wrong. Please try again. If you continue to see this error, please contact support." });
                },
            type = "submit for review";
        // Make a call to run the Ready for QC workflow.
        runWorkflow(workflowId, recordId, successCallback, errorCallback, type);
    }

