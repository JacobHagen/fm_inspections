﻿function getActiveStage(executionContext) {
    debugger;
    var formContext = executionContext.getFormContext(), //Get formContext
        activeProcess = formContext.data.process.getActiveProcess(), //Get Active Process
        bpfStage = "gscserv_bpfstage",
        bpfName = "gscserv_bpfname",
        process1 = "Under Review Business Process Flow",
        process2 = "Final Review Business Process Flow",
        process3 = "Submission Business Process Flow"


    // Get Process Name
    if (activeProcess !== null) {

        var process = activeProcess.getName(),
            processId = formContext.data.process.getInstanceId();
            
        console.log(processId);

        var lookupValue = new Array();
        lookupValue[0] = new Object();
        lookupValue[0].id = processId; // GUID of the lookup id
        lookupValue[0].name = process; // Name of the lookup
        lookupValue[0].entityType = "gscserv_inspectionbusinessprocessflow"; //Entity Type of the lookup entity

        if (process === process1) {
            formContext.getAttribute("gscserv_bpflookup").setValue(lookupValue);
        } else if (process === process2) {
            formContext.getAttribute("gscserv_bpflookupfr").setValue(lookupValue);
        } else if (process === process3) {
            formContext.getAttribute("gscserv_bpflookups").setValue(lookupValue);
        }

    }
}