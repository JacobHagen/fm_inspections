﻿function getAllLoans(executionContext) {
    debugger;
    var formContext = executionContext.getFormContext(),
        loanCount = formContext.getAttribute("gscserv_loancount").getValue(),
        inspectionFlag = formContext.getAttribute("gscserv_inspectionflagreason").getValue(),
        quickViewControl = formContext.ui.quickForms.get("MultipleLiens");

    quickViewControl.setVisible(false);
    formContext.getControl("gscserv_inspectionflagreason").setVisible(false);

    if (inspectionFlag !== null) {
        if (loanCount !== null && loanCount > 1) {
            quickViewControl.setVisible(true);
            formContext.getControl("gscserv_inspectionflagreason").setVisible(false);
        } else if (loanCount !== null && loanCount < 2 && inspectionFlag !== "") {
            quickViewControl.setVisible(false);
            formContext.getControl("gscserv_inspectionflagreason").setVisible(true);
        }
    }
}

function save(executionContext) {
    var formContext = executionContext.getFormContext();
    formContext.data.entity.save();
}