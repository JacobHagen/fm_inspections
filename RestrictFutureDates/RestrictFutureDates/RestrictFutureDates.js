﻿function restrictFutureDates(executionContext) {
    var formContext = executionContext.getFormContext(),
        inspectionDate = formContext.getAttribute("gscserv_dateofinspection").getValue(),
        dateReceivedFromInspector = formContext.getAttribute("gscserv_datereceivedfrominspector").getValue();


    //Checks to see if Actual Date Shipped Field is a date in the future
    if (dateReceivedFromInspector !== null) {

        dateReceivedFromInspector.setHours(0, 0, 0, 0);

        //current date

        if (dateReceivedFromInspector < inspectionDate) {

            var alertStrings = { confirmButtonLabel: "Ok", text: "You cannot enter a date for Date Received from Inspector that is before the Inspection Date." },
                alertOptions = { height: 120, width: 260 };

            Xrm.Navigation.openAlertDialog(alertStrings, alertOptions);

            //Timoeut Function to save the form and show the results of the Restrict Future Dates Workflow
            setTimeout(function () {
                formContext.data.entity.save();
                ;
            }, 500);

        }
    }
    if (inspectionDate === null) {
        var alertStrings = { confirmButtonLabel: "Ok", text: "You must enter an Inspection Date first." },
            alertOptions = { height: 120, width: 260 };

        Xrm.Navigation.openAlertDialog(alertStrings, alertOptions);
    }
}
