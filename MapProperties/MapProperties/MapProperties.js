﻿var _formContext;

function getProperty() {

    //Get record count from the Property Subgrid
    var allRows = _formContext.getControl("Properties").getGrid().getRows().getAll();

    //Function that steps through each row to get their attributes.
    allRows.forEach(function (currentRow) {
        var attributes = currentRow.getData().getEntity().getAttributes().getAll();

        //Function that looks for the purchasePrice field when going through each row.
        attributes.forEach(function (attribute) {

            var attributeName = attribute.getKey();
           
            //If the attribute is the desired field, they will be summed together.
            if (attributeName == "gscserv_propertyid") {

                //Create object containing the property fields info
                var object = new Array();
                object[0] = new Object({
                    id: attribute.getValue()[0].id,
                    name: attribute.getValue()[0].name,
                    entityType: "gscserv_property"
                });

                _formContext.getAttribute("gscserv_gscserv_property").setValue(object);
            }
        });
    })
};

function mapProperties() {
    var allRows = _formContext.getControl("Properties").getGrid().getRows().getAll();

    //null check and check for >1 for subgrid
    if (allRows.length === 0 || allRows > 1) {
        _formContext.getAttribute("gscserv_gscserv_property").setValue(null);
        return;
    }

    if (allRows.length === 1) {
        getProperty();
        _formContext.data.entity.save();
    }
}

function loading(executionContext) {
    _formContext = executionContext.getFormContext(); // get formContext
    setTimeout(function () { mapProperties(); }, 3000);
}