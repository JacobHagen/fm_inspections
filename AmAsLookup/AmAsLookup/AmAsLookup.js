﻿function getQuickViewValue() {
 {
     debugger;
        var quickViewControl = Xrm.Page.ui.quickForms.get("AM");

        if (quickViewControl != undefined) {
            if (quickViewControl.isLoaded()) {
                // Access the value of the attribute bound to the constituent control
                var myattrValue1 = quickViewControl.getControl(0).getAttribute("gscserv_assetmanager").getValue();
                console.log(myattrValue1);

                if (myattrValue1) {

                    //Create object containing the property fields info
                    var object = new Array();
                    object[0] = new Object({
                        id: myattrValue1.getValue()[0].id,
                        name: myattrValue1.getValue()[0].name,
                        entityType: "gscserv_user"
                    })

                    Xrm.Page.getAttribute("gscserv_assetmanager").setValue(object);
                };
            }
            else {
                // Wait for some time and check again
                setTimeout(setAmToLookupField, 10);
            }
        }
        else {
            console.log("No data to display in the quick view control.");
            return;
        }
    }

}

function setAmToLookupField() {
    setTimeout(function () { getQuickViewValue(); }, 3000);
}